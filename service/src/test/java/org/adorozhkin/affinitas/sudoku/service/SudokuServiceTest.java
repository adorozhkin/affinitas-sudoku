package org.adorozhkin.affinitas.sudoku.service;


import org.adorozhkin.affinitas.sudoku.exceptions.GridNotFoundException;
import org.adorozhkin.affinitas.sudoku.exceptions.InvalidGridException;
import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.adorozhkin.affinitas.sudoku.model.Point;
import org.adorozhkin.affinitas.sudoku.model.Status;
import org.adorozhkin.affinitas.sudoku.model.ValidationResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/sudoku-service-test-context.xml"})
public class SudokuServiceTest {
    @Inject
    private SudokuService service;

    private final Point Point = new Point(0, 1);
    private final int Value = 1;

    @Test
    public void testSaveGrid() throws InvalidGridException {
        Grid grid = new Grid(2, new int[][]{
                new int[]{7, 0, 0, 0},
                new int[]{9, 0, 7, 4},
                new int[]{0, 0, 0, 0},
                new int[]{6, 0, 0, 0}
        });

        assertEquals("New grid id is zero", 0, grid.getId());
        Grid savedGrid = service.saveGrid(grid);
        assertNotEquals("Saved grid id is not zero", 0, savedGrid.getId());
        Grid loadedGrid = service.getGrid(savedGrid.getId());
        assertEquals("Saved grid is equals to loaded grid", savedGrid, loadedGrid);
        assertTrue("Saved grid is equals to loaded grid by identity", savedGrid == loadedGrid);
    }

    @Test
    public void testValidMove() throws GridNotFoundException, InvalidGridException {
        Grid defaultGrid = service.getGrid(1);
        assertEquals("Default grid is not corrupted", 0, defaultGrid.getItem(Point));

        Grid newGrid = service.saveGrid(defaultGrid);

        ValidationResult result = service.makeMove(newGrid.getId(), Point, Value);
        assertEquals("Valid move", Status.VALID, result.getStatus());
        Grid savedGrid = service.getGrid(newGrid.getId());
        assertEquals("Value updated", Value, savedGrid.getItem(Point));

        defaultGrid = service.getGrid(1);
        assertEquals("Default grid is not corrupted", 0, defaultGrid.getItem(Point));
    }

    @Test
    public void testInvalidMove() throws GridNotFoundException, InvalidGridException {
        Grid defaultGrid = service.getGrid(1);
        assertEquals("Default grid is not corrupted", 0, defaultGrid.getItem(Point));

        Grid newGrid = service.saveGrid(defaultGrid);

        ValidationResult result = service.makeMove(newGrid.getId(), Point, 7);
        assertEquals("Invalid move", Status.INVALID, result.getStatus());
        Grid savedGrid = service.getGrid(newGrid.getId());
        assertEquals("Value not updated", 0, savedGrid.getItem(Point));

        defaultGrid = service.getGrid(1);
        assertEquals("Default grid is not corrupted", 0, defaultGrid.getItem(Point));
    }

    @Test
    public void testInvalidMoveSave() throws GridNotFoundException, InvalidGridException {
        ((SudokuServiceImpl) service).setSaveInvalidMove(true);
        try {
            Grid defaultGrid = service.getGrid(1);
            Grid newGrid = service.saveGrid(defaultGrid);

            ValidationResult result = service.makeMove(newGrid.getId(), Point, 7);
            assertEquals("Valid move", Status.INVALID, result.getStatus());
            Grid savedGrid = service.getGrid(newGrid.getId());
            assertEquals("Value updated", 7, savedGrid.getItem(Point));
        } finally {
            ((SudokuServiceImpl) service).setSaveInvalidMove(false);
        }
    }
}
