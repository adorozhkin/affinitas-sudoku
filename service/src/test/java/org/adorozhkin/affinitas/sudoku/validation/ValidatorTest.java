package org.adorozhkin.affinitas.sudoku.validation;

import org.adorozhkin.affinitas.sudoku.model.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import javax.inject.Named;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/sudoku-service-test-context.xml"})
public class ValidatorTest {
    @Inject
    @Named("RowValidator")
    private RowValidator rowValidator;

    @Inject
    @Named("ColumnValidator")
    private ColumnValidator colValidator;

    @Inject
    @Named("RegionValidator")
    private RegionValidator regionValidator;

    @Inject
    @Named("GridValidator")
    private GridValidator gridValidator;

    @Test
    public void testValidRow() {
        ValidationResult result = rowValidator.validate(DataUtils.getDefaultGrid().getRow(1));
        assertEquals("Default grid second row should be valid", Status.VALID, result.getStatus());
    }

    @Test
    public void testFinishedRow() {
        ValidationResult result = rowValidator.validate(DataUtils.getFinishedGrid().getRow(1));
        assertEquals("Finished grid second row should be finished", Status.FINISHED, result.getStatus());
    }

    @Test
    public void testInvalidRow() {
        Grid grid = DataUtils.getDefaultGrid();
        grid.setItem(1, 0, 1);
        grid.setItem(1, 7, 1);

        ValidationResult result = rowValidator.validate(grid.getRow(1));
        assertEquals("Invalid grid second row should be invalid", Status.INVALID, result.getStatus());
        assertNotNull("Point should be not null for invalid", result.getPoint());
        assertEquals("Invalid item should be [1, 7]", new Point(1, 7), result.getPoint());
        assertEquals("Invalid type should be row", "Row", result.getValidationType());
    }

    @Test
    public void testValidColumn() {
        ValidationResult result = colValidator.validate(DataUtils.getDefaultGrid().getColumn(1));
        assertEquals("Default grid second column should be valid", Status.VALID, result.getStatus());
    }

    @Test
    public void testFinishedColumn() {
        ValidationResult result = colValidator.validate(DataUtils.getFinishedGrid().getColumn(1));
        assertEquals("Finished grid second column should be finished", Status.FINISHED, result.getStatus());
    }

    @Test
    public void testInvalidColumn() {
        Grid grid = DataUtils.getDefaultGrid();
        grid.setItem(0, 1, 1);
        grid.setItem(7, 1, 1);

        ValidationResult result = colValidator.validate(grid.getColumn(1));
        assertEquals("Invalid grid second column should be valid", Status.INVALID, result.getStatus());
        assertNotNull("Point should be not null for invalid", result.getPoint());
        assertEquals("Invalid item should be [7, 1]", new Point(7, 1), result.getPoint());
        assertEquals("Invalid type should be Column", "Column", result.getValidationType());
    }

    @Test
    public void testValidRegion() {
        ValidationResult result = regionValidator.validate(DataUtils.getDefaultGrid().getRegion(2, 2));
        assertEquals("Default grid [2,2] region should be valid", Status.VALID, result.getStatus());
    }

    @Test
    public void testFinishedRegion() {
        ValidationResult result = regionValidator.validate(DataUtils.getFinishedGrid().getRegion(2, 2));
        assertEquals("Finished grid [2,2] region should be finished", Status.FINISHED, result.getStatus());
    }

    @Test
    public void testInvalidRegion() {
        Grid grid = DataUtils.getDefaultGrid();
        grid.setItem(8, 8, 1);
        grid.setItem(7, 7, 1);

        ValidationResult result = regionValidator.validate(grid.getRegion(2, 2));
        assertEquals("Default grid [2,2] region should be invalid", Status.INVALID, result.getStatus());
        assertEquals("Invalid item should be [8,8]", new Point(8, 8), result.getPoint());
        assertEquals("Invalid type should be Region", "Region", result.getValidationType());
    }

    @Test
    public void testValidGrid() {
        ValidationResult result = gridValidator.validate(DataUtils.getDefaultGrid());
        assertEquals("Default grid should be valid", Status.VALID, result.getStatus());
    }

    @Test
    public void testFinishedGrid() {
        ValidationResult result = gridValidator.validate(DataUtils.getFinishedGrid());
        assertEquals("Finished grid should be finished", Status.FINISHED, result.getStatus());
    }

    @Test
    public void testInvalidGrid() {
        Grid grid = DataUtils.getDefaultGrid();
        grid.setItem(8, 8, 2);
        grid.setItem(7, 7, 2);

        ValidationResult result = gridValidator.validate(grid);
        assertEquals("Invalid grid should be invalid", Status.INVALID, result.getStatus());
        assertEquals("Invalid item should be [8,8]", new Point(8, 8), result.getPoint());
        assertEquals("Invalid type should be Region", "Region", result.getValidationType());
    }

    @Test
    public void testBigGrid() {
        Grid grid = new Grid(9);
        ValidationResult result = gridValidator.validate(grid);
        assertEquals("For big grids should be error", Status.ERROR, result.getStatus());
    }
}
