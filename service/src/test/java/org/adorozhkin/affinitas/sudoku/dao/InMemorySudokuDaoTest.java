package org.adorozhkin.affinitas.sudoku.dao;

import org.adorozhkin.affinitas.sudoku.model.DataUtils;
import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/sudoku-service-test-context.xml"})
public class InMemorySudokuDaoTest {

    @Inject
    private InMemorySudokuDaoImpl dao;

    @Test
    public void testDao() {
        Grid grid = new Grid(2, new int[][]{
                new int[]{7, 0, 0, 0},
                new int[]{9, 0, 7, 4},
                new int[]{0, 0, 0, 0},
                new int[]{6, 0, 0, 0}
        });

        assertEquals("New grid id is zero", 0, grid.getId());
        Grid savedGrid = dao.saveGrid(grid);
        assertNotEquals("Saved grid id is not zero", 0, savedGrid.getId());
        Grid loadedGrid = dao.getGrid(savedGrid.getId());
        assertEquals("Saved grid is equals to loaded grid", savedGrid, loadedGrid);
        assertTrue("Saved grid is equals to loaded grid by identity", savedGrid == loadedGrid);
    }

    @Test
    public void testGetDefaultGrid() {
        Grid grid = dao.getGrid(1);
        assertNotNull("Default grid is not null", grid);
        assertEquals("Default grid equality", DataUtils.getDefaultGrid(), grid);
    }

    @Test
    public void testSaveDefaultGrid() {
        Grid grid = dao.getGrid(1);
        Grid savedGrid = dao.saveGrid(grid);
        assertNotNull("Default grid is saved", savedGrid);
        assertTrue("Id is generated", savedGrid.getId() > 0);
        assertNotEquals("Default grid is saved to new id", DataUtils.getDefaultGrid(), savedGrid);
    }

    @Test
    public void testLRUCache() {
        for (int i = 0; i < 11; i++) {
            Grid grid = dao.getGrid(1);
            grid.setId(i + 2);
            dao.saveGrid(grid);
        }

        Grid shouldBeNull = dao.getGrid(2);
        Grid shouldBeNotNull = dao.getGrid(3);

        assertNull("Grid should be removed from cache", shouldBeNull);
        assertNotNull("Grid should be in cache", shouldBeNotNull);

    }

}
