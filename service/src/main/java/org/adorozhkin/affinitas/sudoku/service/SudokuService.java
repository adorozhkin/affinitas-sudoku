package org.adorozhkin.affinitas.sudoku.service;

import org.adorozhkin.affinitas.sudoku.exceptions.GridNotFoundException;
import org.adorozhkin.affinitas.sudoku.exceptions.InvalidGridException;
import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.adorozhkin.affinitas.sudoku.model.Point;
import org.adorozhkin.affinitas.sudoku.model.ValidationResult;

public interface SudokuService {
    Grid saveGrid(Grid grid) throws InvalidGridException;

    Grid getGrid(long id);

    ValidationResult makeMove(long id, Point move, int value) throws GridNotFoundException;
}
