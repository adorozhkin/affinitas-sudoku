package org.adorozhkin.affinitas.sudoku.validation;

import org.adorozhkin.affinitas.sudoku.model.ValidationResult;
import org.adorozhkin.affinitas.sudoku.view.Column;
import org.adorozhkin.affinitas.sudoku.view.Row;
import org.adorozhkin.affinitas.sudoku.view.VectorView;

/**
 * Validates {@link Row} or {@link Column} objects
 */
public abstract class VectorValidator<T extends VectorView> extends BaseValidator<T> {

    @Override
    ValidationResult doValidation(T item) {
        Validation validation = new Validation(item.getSize());

        for (int i = 0; i < item.getSize(); i++) {
            if (!validation.checkAndSet(item.getItem(i))) {
                return ValidationUtils.getInvalidValidationResult(i, item.getAbsolutePoint(i), getType());
            }
        }

        return ValidationUtils.getSuccessValidationResult(validation);
    }
}
