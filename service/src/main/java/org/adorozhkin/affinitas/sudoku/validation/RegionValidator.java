package org.adorozhkin.affinitas.sudoku.validation;

import org.adorozhkin.affinitas.sudoku.model.ValidationResult;
import org.adorozhkin.affinitas.sudoku.view.Region;

import javax.inject.Named;

/**
 * Validates {@link Region} objects
 */
@Named("RegionValidator")
public class RegionValidator extends BaseValidator<Region> {

    @Override
    ValidationResult doValidation(Region item) {
        Validation validation = new Validation(item.getGrid().getGridSize());

        for (int i = 0; i < item.getSize(); i++) {
            for (int j = 0; j < item.getSize(); j++) {
                if (!validation.checkAndSet(item.getItem(i, j))) {
                    return ValidationUtils.getInvalidValidationResult(i, item.getAbsolutePoint(i, j), getType());
                }
            }
        }

        return ValidationUtils.getSuccessValidationResult(validation);
    }

    public String getType() {
        return "Region";
    }
}
