package org.adorozhkin.affinitas.sudoku.validation;

import org.adorozhkin.affinitas.sudoku.model.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class BaseValidator<T> implements Validator<T> {
    private static final Logger log = LoggerFactory.getLogger(BaseValidator.class);

    public final ValidationResult validate(T item) {
        try {
            return doValidation(item);
        } catch (Exception e) {
            log.error(String.format("Failed to validate item %s", item), e);
            return ValidationUtils.getErrorValidation(e);
        }
    }

    abstract ValidationResult doValidation(T item);
}
