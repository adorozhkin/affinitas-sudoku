package org.adorozhkin.affinitas.sudoku.validation;

/**
 * Stores intermediate validation result. Uses bit logic to collect visited values
 */
class Validation {
    private long mask = 0;
    private final int length;

    public Validation(int length) {
        if (length < 0 || length > 64) {
            throw new IllegalArgumentException("Validation is supported only for grids with size 64");
        }
        this.length = length;
    }

    /**
     * Check value and mark it as used if it wasn't used before.
     *
     * @param value value of item in subsection to check and mark
     * @return {@code true} if {@code value} wasn't marked before, {@code false} otherwise
     */
    boolean checkAndSet(int value) {
        if (value == 0 || (mask & (1 << (value - 1))) == 0) {
            mask |= (1 << (value - 1));
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check whether current result contains completed subsection
     *
     * @return {@code true} if subsection is completed, {@code false} otherwise
     */
    boolean isCompleted() {
        return mask == ((1 << length) - 1);
    }
}
