package org.adorozhkin.affinitas.sudoku.exceptions;

public class InvalidGridException extends Exception {
    public InvalidGridException(String message) {
        super(message);
    }
}
