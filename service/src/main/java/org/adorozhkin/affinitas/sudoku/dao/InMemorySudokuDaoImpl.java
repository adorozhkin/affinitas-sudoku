package org.adorozhkin.affinitas.sudoku.dao;

import org.adorozhkin.affinitas.sudoku.model.DataUtils;
import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

@Named("InMemory")
/**
 * Simple in memory storage to store sudoku grids. It uses LRU cache
 */
public class InMemorySudokuDaoImpl implements SudokuDao {
    private Map<Long, Grid> store;
    private final AtomicInteger idGenerator = new AtomicInteger(1);

    @Value("${inmemory.store.size:100}")
    private int cacheSize;

    @PostConstruct
    public void setup() {
        store = Collections.synchronizedMap(new LinkedHashMap<Long, Grid>() {
            @Override
            protected boolean removeEldestEntry(Map.Entry<Long, Grid> eldest) {
                return size() > cacheSize;
            }
        });
    }

    public Grid saveGrid(Grid grid) {
        if (grid.getId() == 1) {
            return DataUtils.getDefaultGrid();
        }

        if (grid.getId() == 0) {
            grid.setId(idGenerator.incrementAndGet());
        }

        store.put(grid.getId(), grid);
        return grid;
    }

    public Grid getGrid(long id) {
        if (id == 1) {
            return DataUtils.getDefaultGrid();
        }
        return store.get(id);
    }
}
