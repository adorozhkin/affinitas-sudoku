package org.adorozhkin.affinitas.sudoku.service;

import org.adorozhkin.affinitas.sudoku.dao.SudokuDao;
import org.adorozhkin.affinitas.sudoku.exceptions.GridNotFoundException;
import org.adorozhkin.affinitas.sudoku.exceptions.InvalidGridException;
import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.adorozhkin.affinitas.sudoku.model.Point;
import org.adorozhkin.affinitas.sudoku.model.ValidationResult;
import org.adorozhkin.affinitas.sudoku.validation.ValidationUtils;
import org.adorozhkin.affinitas.sudoku.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.inject.Named;

@Service
public class SudokuServiceImpl implements SudokuService {
    private final static Logger log = LoggerFactory.getLogger(SudokuServiceImpl.class);

    @Inject
    @Named("InMemory")
    private SudokuDao dao;

    @Inject
    @Named("GridValidator")
    private Validator<Grid> validator;

    @Value("${save.invalid.move:false}")
    private boolean saveInvalidMove;

    public Grid saveGrid(Grid grid) throws InvalidGridException {
        if (log.isDebugEnabled()) {
            log.debug("Save grid {} - enter", grid);
        }
        try {
            validateGrid(grid);
            return dao.saveGrid(grid);
        } finally {
            if (log.isDebugEnabled()) {
                log.debug("Save grid {} - exit", grid);
            }
        }
    }

    private void validateGrid(Grid grid) throws InvalidGridException {
        if (grid.getRegionSize() < 1) {
            throw new InvalidGridException(String.format("Region size %d is invalid. Should be greater than zero", grid.getRegionSize()));
        }

        if (grid.getGridSize() < grid.getRegionSize()) {
            throw new InvalidGridException(String.format("Grid size %d is invalid. Should be greater or equal to %d", grid.getGridSize(), grid.getRegionSize()));
        }

        for (int i = 0; i < grid.getGridSize(); i++) {
            if (grid.getRowLength(i)!=grid.getGridSize()){
                throw new InvalidGridException(String.format("Row %d size  %d is not equals to grid size (%d)", i, grid.getRowLength(i), grid.getGridSize()));
            }
        }
    }

    public Grid getGrid(long id) {
        if (log.isDebugEnabled()) {
            log.debug("Get grid with id {} - enter", id);
        }
        try {
            return dao.getGrid(id);
        } finally {
            if (log.isDebugEnabled()) {
                log.debug("Get grid with id {} - exit", id);
            }
        }
    }

    public ValidationResult makeMove(long id, Point move, int value) throws GridNotFoundException {
        if (log.isDebugEnabled()) {
            log.debug("Make move {} on grid {} with value {} - enter", move, id, value);
        }
        Grid grid = getGrid(id);
        if (grid == null) {
            throw new GridNotFoundException(id);
        }
        int oldValue = grid.getItem(move);
        try {
            grid.setItem(move, value);
            ValidationResult result = validator.validate(grid);
            if (!ValidationUtils.isNotNullGridValid(result) && !saveInvalidMove) {
                log.info("INVALID move {} detect on grid {} with value {}", move, grid, value);
                grid.setItem(move, oldValue);
            } else {
                saveGrid(grid);
                if (log.isDebugEnabled()) {
                    log.debug("Move {} on grid {} with value {} is saved", move, grid, value);
                }
            }
            return result;
        } catch (Exception e) {
            log.error(String.format("Failed to validate move to %s with value %d on grid %s", move, value, grid), e);
            grid.setItem(move, oldValue);
            return ValidationUtils.getErrorValidation(e);
        } finally {
            if (log.isDebugEnabled()) {
                log.debug("Make move {} on grid {} with value {} - exit", move, id, value);
            }
        }
    }

    public boolean isSaveInvalidMove() {
        return saveInvalidMove;
    }

    public void setSaveInvalidMove(boolean saveInvalidMove) {
        this.saveInvalidMove = saveInvalidMove;
    }
}
