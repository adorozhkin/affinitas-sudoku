package org.adorozhkin.affinitas.sudoku.validation;

import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.adorozhkin.affinitas.sudoku.model.ValidationResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;

@Named("GridValidator")
public class GridValidator extends BaseValidator<Grid> {

    private final static Logger log = LoggerFactory.getLogger(GridValidator.class);

    @Inject
    @Named("RowValidator")
    private RowValidator rowValidator;

    @Inject
    @Named("ColumnValidator")
    private ColumnValidator columnValidator;

    @Inject
    @Named("RegionValidator")
    private RegionValidator regionValidator;

    ValidationResult doValidation(Grid grid) {
        if (log.isDebugEnabled()) {
            log.debug("Validating grid {} -  enter", grid);
        }
        try {
            ValidationResult gridResult = null;

            for (int i = 0; i < grid.getGridSize() && ValidationUtils.isGridValid(gridResult); i++) {
                gridResult = checkResult(rowValidator.validate(grid.getRow(i)), gridResult);
            }

            for (int i = 0; i < grid.getGridSize() && ValidationUtils.isGridValid(gridResult); i++) {
                gridResult = checkResult(columnValidator.validate(grid.getColumn(i)), gridResult);
            }

            for (int i = 0; i < grid.getRegionSize() && ValidationUtils.isGridValid(gridResult); i++) {
                for (int j = 0; j < grid.getRegionSize() && ValidationUtils.isGridValid(gridResult); j++) {
                    gridResult = checkResult(regionValidator.validate(grid.getRegion(i, j)), gridResult);
                }
            }
            return gridResult;
        } finally {
            if (log.isDebugEnabled()) {
                log.debug("Validating grid {} -  exit", grid);
            }
        }
    }

    private ValidationResult checkResult(ValidationResult current, ValidationResult finalResult) {
        switch (current.getStatus()) {
            case INVALID:
            case ERROR:
                return current;
            case FINISHED:
                if (finalResult == null) {
                    return current;
                }
                break;
            case VALID:
                return current;
        }
        return null;
    }

    public String getType() {
        return "Grid";
    }
}
