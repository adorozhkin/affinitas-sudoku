package org.adorozhkin.affinitas.sudoku.validation;

import org.adorozhkin.affinitas.sudoku.view.Column;

import javax.inject.Named;

@Named("ColumnValidator")
public class ColumnValidator extends VectorValidator<Column> {
    public String getType() {
        return "Column";
    }
}
