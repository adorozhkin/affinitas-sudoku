package org.adorozhkin.affinitas.sudoku.dao;

import org.adorozhkin.affinitas.sudoku.model.Grid;

public interface SudokuDao {
    Grid saveGrid(Grid grid);

    Grid getGrid(long id);
}
