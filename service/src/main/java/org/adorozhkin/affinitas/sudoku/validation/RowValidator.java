package org.adorozhkin.affinitas.sudoku.validation;

import org.adorozhkin.affinitas.sudoku.view.Row;

import javax.inject.Named;

@Named("RowValidator")
public class RowValidator extends VectorValidator<Row> {
    public String getType() {
        return "Row";
    }
}
