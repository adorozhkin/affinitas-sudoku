package org.adorozhkin.affinitas.sudoku.validation;

import org.adorozhkin.affinitas.sudoku.model.ValidationResult;

public interface Validator<T> {
    ValidationResult validate(T item);

    String getType();
}
