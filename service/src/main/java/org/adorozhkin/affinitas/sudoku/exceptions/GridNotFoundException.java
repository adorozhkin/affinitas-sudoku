package org.adorozhkin.affinitas.sudoku.exceptions;

/**
 * Created by Лена on 15.12.2015.
 */
public class GridNotFoundException extends Exception {
    private final long id;

    public GridNotFoundException(long id) {
        this.id = id;
    }

    @Override
    public String getMessage() {
        return String.format("Grid with id %d is not found", id);
    }
}
