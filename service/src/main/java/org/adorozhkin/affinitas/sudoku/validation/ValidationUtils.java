package org.adorozhkin.affinitas.sudoku.validation;

import org.adorozhkin.affinitas.sudoku.model.Point;
import org.adorozhkin.affinitas.sudoku.model.Status;
import org.adorozhkin.affinitas.sudoku.model.ValidationResult;

public class ValidationUtils {
    public static ValidationResult getSuccessValidationResult(Validation validation) {
        if (validation.isCompleted()) {
            return new ValidationResult(Status.FINISHED, "Sudoku grid is finished");
        } else {
            return new ValidationResult(Status.VALID, "Sudoku grid is valid");
        }
    }

    public static ValidationResult getInvalidValidationResult(int value, Point point, String type) {
        return new ValidationResult(Status.INVALID, new Point(point.getRow(), point.getColumn()), type,
                String.format("Duplicate value %d found at [%d, %d]", value, point.getRow(), point.getColumn()));
    }

    public static ValidationResult getErrorValidation(Exception e) {
        return new ValidationResult(Status.ERROR, e.getMessage());
    }

    public static boolean isGridValid(ValidationResult gridResult) {
        return gridResult == null || isNotNullGridValid(gridResult);
    }

    public static boolean isNotNullGridValid(ValidationResult gridResult) {
        return gridResult.getStatus() != Status.ERROR && gridResult.getStatus() != Status.INVALID;
    }
}
