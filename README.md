# AFFINITAS SUDOKU CODING TASK#

Simple REST interface to validate moves to Sudoku grid.

This project can be build with Java 1.5 and above using Maven.


```
#!shell

mvn clean install 
```

This will build project, execute unit tests and generate WAR file.


```
#!shell

rest/target/rest-0.0.1-SNAPSHOT.war
```
 file can be renamed in any appropriate name and deployed to any servlet container.

WAR file is shipped with Swagger UI for easy understanding and manual testing of REST interface. Just navigate your browser to root of your deployment. Grid id 1 has a special meaning. It always return new copy of default grid specified in task.

Project was tested agains Java 1.8, Maven 3.3.9 and Tomcat 8.0