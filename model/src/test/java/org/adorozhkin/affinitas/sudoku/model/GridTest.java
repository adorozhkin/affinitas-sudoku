package org.adorozhkin.affinitas.sudoku.model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GridTest {

    public static final int DefaultRegionSize = 3;
    public static final int DefaultGridSize = DefaultRegionSize * DefaultRegionSize;

    @Test
    public void testEmptytGrid() {
        Grid grid = new Grid(DefaultRegionSize);
        for (int i = 0; i < grid.getGridSize(); i++) {
            for (int j = 0; j < grid.getGridSize(); j++) {
                assertEquals(String.format("Item %d,%d should be zero", i, j), 0, grid.getItem(i, j));
            }
        }
    }

    @Test
    public void testDefaultGrid() {
        Grid grid = DataUtils.getDefaultGrid();
        assertEquals("Item [0,0] check", 7, grid.getItem(0, 0));
        assertEquals("Item [8,8] check", 0, grid.getItem(8, 8));
        assertEquals("Item [3,2] check", 9, grid.getItem(3, 2));
    }

    @Test
    public void testSetItem() {
        Grid grid = new Grid(DefaultRegionSize);
        assertEquals("Item [1,2] check", 0, grid.getItem(1, 2));
        grid.setItem(1, 2, 3);
        assertEquals("Item [1,2] check", 3, grid.getItem(1, 2));
        grid.setItem(1, 2, 0);
        assertEquals("Item [1,2] check", 0, grid.getItem(1, 2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetValidationRow() {
        Grid grid = new Grid(DefaultRegionSize);
        grid.getItem(DefaultGridSize, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetValidationCol() {
        Grid grid = new Grid(DefaultRegionSize);
        grid.getItem(0, DefaultGridSize);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetNegativeValidation() {
        Grid grid = new Grid(DefaultRegionSize);
        grid.getItem(0, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetValidationRow() {
        Grid grid = new Grid(DefaultRegionSize);
        grid.setItem(DefaultGridSize, 0, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetValidationCol() {
        Grid grid = new Grid(DefaultRegionSize);
        grid.setItem(DefaultGridSize, 0, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetValidationValue() {
        Grid grid = new Grid(DefaultRegionSize);
        grid.setItem(0, 0, DefaultGridSize + 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetValidationNegativeValue() {
        Grid grid = new Grid(DefaultRegionSize);
        grid.setItem(0, 0, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationInitRows() {
        new Grid(2, new int[][]{
                new int[]{1, 2}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidationInitCols() {
        new Grid(2, new int[][]{
                new int[]{1, 2},
                new int[]{3},
        });
    }
}
