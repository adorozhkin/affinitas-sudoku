package org.adorozhkin.affinitas.sudoku.view;

import org.adorozhkin.affinitas.sudoku.model.Grid;

/**
 * Base class for all views that simplifies access to {@link Grid} subsection (e.g. {@link Region}
 */
public abstract class View {
    protected final String type;
    private final Grid grid;

    View(Grid grid, String type) {
        this.grid = grid;
        this.type = type;
    }

    public Grid getGrid() {
        return grid;
    }

    public String getType() {
        return type;
    }
}
