package org.adorozhkin.affinitas.sudoku.model;

import java.io.Serializable;

/**
 * Simple container of item's coordinates
 */
public class Point implements Serializable {
    private int row;
    private int column;

    public Point() {
    }

    public Point(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        return row == point.row && column == point.column;

    }

    @Override
    public int hashCode() {
        int result = row;
        result = 31 * result + column;
        return result;
    }

    @Override
    public String toString() {
        return "Point{" +
                "row=" + row +
                ", column=" + column +
                '}';
    }
}
