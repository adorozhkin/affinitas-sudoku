package org.adorozhkin.affinitas.sudoku.view;


import org.adorozhkin.affinitas.sudoku.model.DataUtils;
import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.adorozhkin.affinitas.sudoku.model.Point;

/**
 * Abstract class for single dimension view of {@link Grid}'s subsection (e.g. {@link Column}, {@link Row})
 */
public abstract class VectorView extends View {
    private final int index;

    VectorView(Grid grid, int index, String type) {
        super(grid, type);
        DataUtils.validateInput(index, type, 0, grid.getGridSize() - 1);
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public int getSize() {
        return getGrid().getGridSize();
    }

    /**
     * @param i index of item of current vector to be returned. Valid values 0..{@link Grid#getGridSize}-1
     * @return Returns item of current vector specified by rowIndex {@code i}
     */
    public int getItem(int i) {
        DataUtils.validateInput(i, "Index", 0, getGrid().getGridSize() - 1);
        Point abs = getAbsolutePoint(i);
        return getGrid().getItem(abs.getRow(), abs.getColumn());
    }

    public abstract Point getAbsolutePoint(int index);

    @Override
    public String toString() {
        return getType() + " of grid " + getGrid();
    }
}
