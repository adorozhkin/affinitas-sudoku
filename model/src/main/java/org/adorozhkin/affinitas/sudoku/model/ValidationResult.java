package org.adorozhkin.affinitas.sudoku.model;

import java.io.Serializable;

/**
 * Holds result of {@link Grid}
 * {@link org.adorozhkin.affinitas.sudoku.view.Row}
 * {@link org.adorozhkin.affinitas.sudoku.view.Column}
 * or {@link org.adorozhkin.affinitas.sudoku.view.Region} validation
 */
public class ValidationResult implements Serializable {
    private Status status;
    /**
     * Coordinates where first invalid item detected
     */
    private Point point;

    /**
     * Message for invalid item or error message from exception
     */
    private String message;
    /**
     * Type of validator that detected invalid item
     */
    private String validationType;

    private ValidationResult() {
    }

    public ValidationResult(Status status, String message) {
        this(status, null, null, message);
    }

    public ValidationResult(Status status, Point point, String validationType, String message) {
        this.status = status;
        this.point = point;
        this.message = message;
        this.validationType = validationType;
    }

    public Status getStatus() {
        return status;
    }

    public Point getPoint() {
        return point;
    }

    public String getValidationType() {
        return validationType;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ValidationResult{" +
                "status=" + status +
                ", point=" + point +
                ", message='" + message + '\'' +
                '}';
    }
}
