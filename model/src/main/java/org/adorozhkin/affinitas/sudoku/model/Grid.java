package org.adorozhkin.affinitas.sudoku.model;

/**
 * Class represents whole Sudoku game grid.
 * Grid consist of {@link Row}, {@link Column} and {@link Region}
 * See <a href="https://en.wikipedia.org/wiki/Sudoku">Wikipedia</a> for more info
 */

import io.swagger.annotations.ApiModelProperty;
import org.adorozhkin.affinitas.sudoku.view.Column;
import org.adorozhkin.affinitas.sudoku.view.Region;
import org.adorozhkin.affinitas.sudoku.view.Row;
import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.io.Serializable;

@JsonAutoDetect(isGetterVisibility = JsonAutoDetect.Visibility.NONE,
        fieldVisibility = JsonAutoDetect.Visibility.ANY,
        getterVisibility = JsonAutoDetect.Visibility.NONE,
        setterVisibility = JsonAutoDetect.Visibility.NONE)
public class Grid implements Serializable {
    private long id;
    private int regionSize;
    private int[][] grid;

    private Grid() {
    }

    public Grid(int regionSize) {
        this.regionSize = regionSize;
        this.grid = initializeGrid(regionSize * regionSize);
    }

    public Grid(int regionSize, int[][] initial) {
        this(regionSize);
        DataUtils.validateInput(initial.length, "Number of rows", initial.length, initial.length);
        for (int i = 0; i < initial.length; i++) {
            int[] row = initial[i];
            DataUtils.validateInput(row.length, "Row Size", initial.length, initial.length);
            System.arraycopy(initial[i], 0, this.grid[i], 0, row.length);
        }
    }

    private int[][] initializeGrid(int regionSize) {
        int[][] result = new int[regionSize][];
        for (int i = 0; i < regionSize; i++) {
            result[i] = new int[regionSize];
        }
        return result;
    }

    @ApiModelProperty(required = true)
    public int getRegionSize() {
        return regionSize;
    }

    @ApiModelProperty(hidden = true)
    public int getGridSize() {
        return grid == null ? 0 : grid.length;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getRowLength(int i) {
        return grid[i].length;
    }

    /**
     * @param row row of item to be returned. Valid values 0..{@code grid.length-1}
     * @param col column of item to be returned. Valid values 0..{@code grid.length-1}
     * @return item of sudoku's grid specified by {@code row} and {@code row}
     */
    public int getItem(int row, int col) {
        DataUtils.validateInput(row, "Row", 0, getGridSize() - 1);
        DataUtils.validateInput(col, "Column", 0, getGridSize() - 1);
        return grid[row][col];
    }

    public int getItem(Point point) {
        return getItem(point.getRow(), point.getColumn());
    }

    /**
     * Sets value in sudoku grid
     *
     * @param row   row of item to be set. Valid values 0..{@code gridSize-1}
     * @param col   column of item to be returned. Valid values 0..{@code gridSize-1}
     * @param value value to be set. Valid values 0..{@code gridSize}. Where 0 means undefined value (e.g. to remove last move)
     */
    public void setItem(int row, int col, int value) {
        DataUtils.validateInput(row, "Row", 0, getGridSize() - 1);
        DataUtils.validateInput(col, "Column", 0, getGridSize() - 1);
        DataUtils.validateInput(value, "Value", 0, getGridSize());
        grid[row][col] = value;
    }

    @ApiModelProperty(required = true)
    public int[][] getGrid() {
        throw new UnsupportedOperationException("Only for Swagger Model Schema");
    }

    public void setItem(Point point, int value) {
        setItem(point.getRow(), point.getColumn(), value);
    }

    public Row getRow(int index) {
        return new Row(this, index);
    }

    public Column getColumn(int index) {
        return new Column(this, index);
    }

    public Region getRegion(int row, int col) {
        return new Region(this, row, col);
    }

    public Region getRegion(Point point) {
        return getRegion(point.getRow(), point.getColumn());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Grid grid = (Grid) o;

        return id == grid.id;

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "Grid{" +
                "id=" + id +
                ", regionSize=" + regionSize +
                ", gridSize=" + grid.length +
                '}';
    }
}
