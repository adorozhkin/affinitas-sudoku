package org.adorozhkin.affinitas.sudoku.model;

/**
 * Validate status
 */
public enum Status {
    VALID, INVALID, FINISHED, ERROR;
}
