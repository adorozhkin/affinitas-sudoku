package org.adorozhkin.affinitas.sudoku.view;

import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.adorozhkin.affinitas.sudoku.model.Point;

/**
 * Separated row of {@link Grid} with zero-based coordinate {@link VectorView#index}
 */
public class Row extends VectorView {
    public Row(Grid grid, int index) {
        super(grid, index, "Row[" + index + "]");
    }

    @Override
    public Point getAbsolutePoint(int index) {
        return new Point(getIndex(), index);
    }
}
