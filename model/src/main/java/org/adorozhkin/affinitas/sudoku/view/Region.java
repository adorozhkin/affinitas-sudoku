package org.adorozhkin.affinitas.sudoku.view;

import org.adorozhkin.affinitas.sudoku.model.DataUtils;
import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.adorozhkin.affinitas.sudoku.model.Point;

/**
 * Separated region of {@link Grid} with zero-based coordinate {@link Region#gridRow} and {@link Region#gridCol}
 */
public class Region extends View {

    private final int gridRow;
    private final int gridCol;

    public Region(Grid grid, int row, int col) {
        super(grid, "Region");
        DataUtils.validateInput(row, "Row", 0, grid.getRegionSize() - 1);
        DataUtils.validateInput(col, "Column", 0, grid.getRegionSize() - 1);
        this.gridRow = row;
        this.gridCol = col;
    }

    public int getRow() {
        return gridRow;
    }

    public int getColumn() {
        return gridCol;
    }

    public int getSize() {
        return getGrid().getRegionSize();
    }

    /**
     * @param row row of item inside current region to be returned. Valid values 0..{@link Grid#regionSize}-1
     * @param col col of item inside current region to be returned. Valid values 0..{@link Grid#regionSize}-1
     * @return Returns item of current region specified by relative coordinates {@code row} and {@code row}
     */
    public int getItem(int row, int col) {
        DataUtils.validateInput(row, "Row", 0, getGrid().getRegionSize() - 1);
        DataUtils.validateInput(col, "Column", 0, getGrid().getRegionSize() - 1);
        Point abs = getAbsolutePoint(row, col);
        return getGrid().getItem(abs.getRow(), abs.getColumn());
    }

    public Point getAbsolutePoint(int row, int col) {
        return new Point(gridRow * getGrid().getRegionSize() + row, gridCol * getGrid().getRegionSize() + col);
    }

    @Override
    public String toString() {
        return "Region{" + getRow() + "," + getColumn() + "} of grid " + getGrid();
    }
}
