package org.adorozhkin.affinitas.sudoku.view;

import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.adorozhkin.affinitas.sudoku.model.Point;

/**
 * Separated column of {@link Grid} with zero-based coordinate {@link VectorView#index}
 */
public class Column extends VectorView {
    public Column(Grid grid, int index) {
        super(grid, index, "Column[" + index + "]");
    }

    @Override
    public Point getAbsolutePoint(int index) {
        return new Point(index, getIndex());
    }
}
