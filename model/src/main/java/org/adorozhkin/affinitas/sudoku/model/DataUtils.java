package org.adorozhkin.affinitas.sudoku.model;

public class DataUtils {
    public static void validateInput(int value, String name, int min, int max) {
        if (value < min || value > max) {
            throw new IllegalArgumentException(String.format("%s value %d is out of range. Expected values are %d..%d", name, value, min, max));
        }
    }

    public static Grid getDefaultGrid() {
        return new Grid(3, new int[][]{
                new int[]{7, 0, 0, 0, 4, 0, 5, 3, 0},
                new int[]{0, 0, 5, 0, 0, 8, 0, 1, 0},
                new int[]{0, 0, 8, 5, 0, 9, 0, 4, 0},
                new int[]{5, 3, 9, 0, 6, 0, 0, 0, 1},
                new int[]{0, 0, 0, 0, 1, 0, 0, 0, 5},
                new int[]{8, 0, 0, 7, 2, 0, 9, 0, 0},
                new int[]{9, 0, 7, 4, 0, 0, 0, 0, 0},
                new int[]{0, 0, 0, 0, 5, 7, 0, 0, 0},
                new int[]{6, 0, 0, 0, 0, 0, 0, 5, 0}
        });
    }

    public static Grid getFinishedGrid() {
        return new Grid(3, new int[][]{
                new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9},
                new int[]{4, 5, 6, 7, 8, 9, 1, 2, 3},
                new int[]{7, 8, 9, 1, 2, 3, 4, 5, 6},
                new int[]{2, 3, 4, 5, 6, 7, 8, 9, 1},
                new int[]{5, 6, 7, 8, 9, 1, 2, 3, 4},
                new int[]{8, 9, 1, 2, 3, 4, 5, 6, 7},
                new int[]{3, 4, 5, 6, 7, 8, 9, 1, 2},
                new int[]{6, 7, 8, 9, 1, 2, 3, 4, 5},
                new int[]{9, 1, 2, 3, 4, 5, 6, 7, 8}
        });
    }
}
