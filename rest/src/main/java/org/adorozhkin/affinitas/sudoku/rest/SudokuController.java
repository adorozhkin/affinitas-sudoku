package org.adorozhkin.affinitas.sudoku.rest;

import io.swagger.annotations.*;
import org.adorozhkin.affinitas.sudoku.exceptions.GridNotFoundException;
import org.adorozhkin.affinitas.sudoku.exceptions.InvalidGridException;
import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.adorozhkin.affinitas.sudoku.model.Point;
import org.adorozhkin.affinitas.sudoku.model.Status;
import org.adorozhkin.affinitas.sudoku.model.ValidationResult;
import org.adorozhkin.affinitas.sudoku.service.SudokuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
@Controller
@Produces(MediaType.APPLICATION_JSON)
@Api(value = "/sudoku", description = "Endpoint to get, upload and validate sudoku grid", produces = MediaType.APPLICATION_JSON)
public class SudokuController {

    @Inject
    private SudokuService service;

    private final static Logger log = LoggerFactory.getLogger(SudokuController.class);

    @GET
    @Path("/grid/{id}")
    @ApiOperation(value = "Find sudoku grid by id", notes = "Grid with id 1 is default grid",
            response = Grid.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Grid is found"),
            @ApiResponse(code = 404, message = "Grid with provided id is not found"),
            @ApiResponse(code = 500, message = "In case of any error")})
    public Response getGrid(@ApiParam(value = "Id of grid to be loaded") @PathParam("id") long id) {
        log.info("Get grid by id {}, id");
        Grid grid = service.getGrid(id);
        if (grid == null) {
            return Response.status(404).entity(String.format("\"Grid with id %s is not found\"", id)).build();
        } else {
            return Response.status(200).entity(grid).build();
        }
    }

    @PUT
    @Path("grid")
    @ApiOperation(value = "Save grid. For new grids unique id is generated")
    @ApiResponses({@ApiResponse(code = 200, message = "Grid is found"),
            @ApiResponse(code = 400, message = "In case of invalid input format"),
            @ApiResponse(code = 500, message = "In case of any error")})
    @Consumes(MediaType.APPLICATION_JSON)
    public Grid saveGrid(@ApiParam("Grid to be saved") Grid grid) throws InvalidGridException {
        log.info("Saving grid {}", grid);
        return service.saveGrid(grid);
    }

    @POST
    @Path("move/{id}/{row}/{col}/{value}")
    @ApiOperation(value = "Make move of sudoku grid", response = ValidationResult.class,
            notes = "By default invalid moves are not persisted. But this can be changed on Sudoku Service configuration level")
    @ApiResponses({@ApiResponse(code = 200, message = "Grid is found"),
            @ApiResponse(code = 404, message = "Grid with provided id is not found"),
            @ApiResponse(code = 500, message = "In case of any error")})
    public Response makeMove(@PathParam("id") long id,
                             @PathParam("row") int row,
                             @PathParam("col") int col,
                             @PathParam("value") int value) {
        log.info("Validate move for grid id {} row {} column {} value {}", id, row, col, value);
        try {
            ValidationResult result = service.makeMove(id, new Point(row, col), value);
            if (result.getStatus() == Status.ERROR) {
                return Response.status(500).entity(result.getMessage()).build();
            }
            return Response.status(200).entity(result).build();
        } catch (GridNotFoundException e) {
            log.error(String.format("\"Grid with id %d is not found\"", id), e);
            return Response.status(404).entity(e.getMessage()).build();
        }
    }
}
