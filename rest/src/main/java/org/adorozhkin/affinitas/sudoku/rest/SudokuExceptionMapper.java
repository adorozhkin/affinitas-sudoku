package org.adorozhkin.affinitas.sudoku.rest;

import org.adorozhkin.affinitas.sudoku.exceptions.InvalidGridException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class SudokuExceptionMapper implements ExceptionMapper<InvalidGridException> {

    public Response toResponse(InvalidGridException e) {
        return Response.status(500).entity(e.getMessage()).build();
    }
}
