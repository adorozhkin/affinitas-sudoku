package org.adorozhkin.affinitas.sudoku.rest;

import org.adorozhkin.affinitas.sudoku.model.DataUtils;
import org.adorozhkin.affinitas.sudoku.model.Grid;
import org.adorozhkin.affinitas.sudoku.model.Status;
import org.adorozhkin.affinitas.sudoku.model.ValidationResult;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring/application-context-test.xml"})
@WebAppConfiguration
public class SudokuControllerTest {

    private Dispatcher dispatcher;

    @Inject
    private SudokuController controller;

    private static final ObjectMapper om = new ObjectMapper();

    @Before
    public void setup() {
        dispatcher = MockDispatcherFactory.createDispatcher();
        dispatcher.getRegistry().addSingletonResource(controller);
        ResteasyProviderFactory.getInstance().registerProvider(SudokuExceptionMapper.class);
    }

    @Test
    public void testGetGrid() throws Exception {
        getGrid(1);
    }

    private Grid getGrid(long id) throws URISyntaxException, IOException {
        MockHttpRequest request = MockHttpRequest.get(String.format("/grid/%d", id));
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        assertEquals("Http status 200", HttpServletResponse.SC_OK, response.getStatus());
        assertNotNull("Content is not empty", response.getContentAsString());
        List<MediaType> contentType = new ArrayList<MediaType>();
        contentType.add(MediaType.APPLICATION_JSON_TYPE);
        assertEquals("Content type", contentType, response.getOutputHeaders().get(HttpHeaders.CONTENT_TYPE));
        Grid grid = om.readValue(response.getContentAsString(), Grid.class);
        assertNotNull("Grid is converted from JSON", grid);
        return grid;
    }

    @Test
    public void testGetNonExistingGrid() throws Exception {
        MockHttpRequest request = MockHttpRequest.get("/grid/1000");
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        assertEquals("Http status 404", HttpServletResponse.SC_NOT_FOUND, response.getStatus());
    }

    @Test
    public void testSaveGrid() throws Exception {
        createGrid();
    }

    @Test
    public void testMakeMove() throws Exception {
        Grid grid = createGrid();
        MockHttpRequest request = MockHttpRequest
                .post(String.format("/move/%d/%d/%d/%d", grid.getId(), 0, 1, 2))
                .contentType(MediaType.APPLICATION_JSON_TYPE);
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        assertEquals("Http status 200", HttpServletResponse.SC_OK, response.getStatus());
        ValidationResult result = om.readValue(response.getContentAsString(), ValidationResult.class);
        assertEquals("Valid move", Status.VALID, result.getStatus());

        Grid savedGrid = getGrid(grid.getId());
        assertEquals("Move is saved", 2, savedGrid.getItem(0, 1));
    }

    @Test
    public void testMakeInvalidMove() throws Exception {
        Grid grid = createGrid();
        MockHttpRequest request = MockHttpRequest
                .post(String.format("/move/%d/%d/%d/%d", grid.getId(), 0, 1, 7))
                .contentType(MediaType.APPLICATION_JSON_TYPE);
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        assertEquals("Http status 200", HttpServletResponse.SC_OK, response.getStatus());
        ValidationResult result = om.readValue(response.getContentAsString(), ValidationResult.class);
        assertEquals("Invalid move", Status.INVALID, result.getStatus());
        assertEquals("Row validation", "Row", result.getValidationType());

        Grid savedGrid = getGrid(grid.getId());
        assertEquals("Move is not saved", 0, savedGrid.getItem(0, 1));
    }

    @Test
    public void testMakeMoveOnUnknownGrid() throws Exception {
        MockHttpRequest request = MockHttpRequest
                .post(String.format("/move/%d/%d/%d/%d", 1000, 0, 1, 7))
                .contentType(MediaType.APPLICATION_JSON_TYPE);
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        assertEquals("Http status 404", HttpServletResponse.SC_NOT_FOUND, response.getStatus());
    }

    @Test
    public void testMakeIllegalMove() throws Exception {
        Grid grid = createGrid();
        MockHttpRequest request = MockHttpRequest
                .post(String.format("/move/%d/%d/%d/%d", grid.getId(), 0, 1, 700))
                .contentType(MediaType.APPLICATION_JSON_TYPE);
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        assertEquals("Http status 500", HttpServletResponse.SC_INTERNAL_SERVER_ERROR, response.getStatus());
    }

    @Test
    public void testNonJsonPut() throws Exception {
        MockHttpRequest request = MockHttpRequest
                .put("/grid")
                .contentType(MediaType.APPLICATION_JSON_TYPE)
                .content("Not a Json [ at all]".getBytes());
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        assertEquals("Http status 400", HttpServletResponse.SC_BAD_REQUEST, response.getStatus());
    }

    @Test
    public void testSaveInvalidGrid() throws URISyntaxException {
        MockHttpRequest request = MockHttpRequest
                .put("/grid")
                .contentType(MediaType.APPLICATION_JSON_TYPE)
                .content(("{\"regionSize\": 2, \"grid\": [[0, 0],[0, 0],[0, 0],[0, 0]]}").getBytes());
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        assertEquals("Http status 500", HttpServletResponse.SC_INTERNAL_SERVER_ERROR, response.getStatus());

    }

    private Grid createGrid() throws URISyntaxException, IOException {
        Grid grid = DataUtils.getDefaultGrid();

        MockHttpRequest request = MockHttpRequest
                .put("/grid")
                .contentType(MediaType.APPLICATION_JSON_TYPE)
                .content(om.writeValueAsBytes(grid));
        MockHttpResponse response = new MockHttpResponse();
        dispatcher.invoke(request, response);
        assertEquals("Http status 200", HttpServletResponse.SC_OK, response.getStatus());
        Grid savedGrid = om.readValue(response.getContentAsString(), Grid.class);
        assertNotNull("Saved grid is not null", savedGrid);
        long id = savedGrid.getId();
        assertTrue("Generated id greater than zero", id > 0);
        return savedGrid;
    }
}
